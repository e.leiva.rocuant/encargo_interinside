# Encargo_interinside

Descripcion:
Aplicacion para el manejo de clientes con multiples cuentas, con dos vistas, una de cliente y otra de administrador. El cliente tiene opciones de visualizacion, mientras el administrador puede crear, editar, eliminar y modificar tanto clientes como sus cuentas.

## Tecnologias usadas:

* En el back-end:
  - Java spring
  - Mysql
----
* En el front-end:
    * Angular

## El diseño de la base de datos es la siguiente:
![diseño base de datos](https://gitlab.com/e.leiva.rocuant/encargo_interinside/-/raw/master/Back-Spring-V1.0/Dise%C3%B1o%20de%20base%20de%20datos.PNG)

## Algunas imagenes del proyecto funcionando:

### Vista cliente:
![vista cliente](https://gitlab.com/e.leiva.rocuant/encargo_interinside/-/raw/master/front-angular-V0/imagenes%20funcion/Vista-cliente.PNG)

### Vista administrador:

#### Vista administrador:

![vista administrador](https://gitlab.com/e.leiva.rocuant/encargo_interinside/-/raw/master/front-angular-V0/imagenes%20funcion/Vista-administrador.PNG)

#### Vista administrador creacion cuenta:

![Vista administrador creacion cuenta](https://gitlab.com/e.leiva.rocuant/encargo_interinside/-/raw/master/front-angular-V0/imagenes%20funcion/Vista-administrador-creacion-cliente.PNG)

#### Vista administrador creacion cuenta:

![vista administrador creacion cuenta](https://gitlab.com/e.leiva.rocuant/encargo_interinside/-/raw/master/front-angular-V0/imagenes%20funcion/Vista-administrador-creacion-cuenta.PNG)

#### Vista admnistrador feedback noClientes:

![vista admnistrador feedback noClientes](https://gitlab.com/e.leiva.rocuant/encargo_interinside/-/raw/master/front-angular-V0/imagenes%20funcion/Vista-administrador-feedback-noClientes.PNG)

#### Vista administrador modificacion cuenta:

![vista administrador modificacion cuenta](https://gitlab.com/e.leiva.rocuant/encargo_interinside/-/raw/master/front-angular-V0/imagenes%20funcion/Vista-administrador-modificacion-cuenta.PNG)

#### Vista administrador modificacion cliente:

![vista administrador modificacion cliente](https://gitlab.com/e.leiva.rocuant/encargo_interinside/-/raw/master/front-angular-V0/imagenes%20funcion/Vista-administrador-modificacion-cliente.PNG)
