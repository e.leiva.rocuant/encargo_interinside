import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  clientes: any[] = [];
  cuentas: any[] = [];
  cuenta: any = null ;
  alerta = '';
  alertaAdvertencia = '';
  constructor() { }

  ngOnInit(): void {
    fetch('http://localhost:8080/cliente',{
      method: 'GET',
      redirect: 'follow'
    })
    .then(response => response.json())
    .then(data => {
      data.map(date => {date.desplegado = false; this.clientes.push(date); });
    } )
    .catch(err => this.alerta = err);
  }

  mostrarInformacion(){
    console.log(this.clientes);
  }

  traerCuentasCliente(eventCliente){
    const idCliente = eventCliente.target.value;
    if(idCliente !== "-----" ){
      fetch(`http://localhost:8080/cuenta/cliente/${idCliente}`,{
        method: 'GET',
        redirect: 'follow'
      })
      .then(response => response.json())
      .then(data => {
        this.cuentas = data;
        if(this.cuentas.length === 0){
          this.alertaAdvertencia = "Este cliente no tiene cuentas";
        }else{
          this.alertaAdvertencia = "";
        }
      } )
      .catch(err => this.alerta = err);
    }else{
      this.cuentas = [];
      this.cuenta = null;
    }

  }

  elegirCuenta(eventCuenta){
    const idCuenta = eventCuenta.target.value;
    if(idCuenta !== "-----"){
      fetch(`http://localhost:8080/cuenta/${idCuenta}`,{
        method: 'GET',
        redirect: 'follow'
      })
      .then(response => response.json())
      .then(data => {
        this.cuenta = data;
      } )
      .catch(err => this.alerta = err);
    }else{
      this.cuenta = null;
    }
  }
}
