import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-administrador',
  templateUrl: './administrador.component.html',
  styleUrls: ['./administrador.component.css']
})
export class AdministradorComponent implements OnInit {
  clientes: any[] = [];
  alerta = '';
  alertaAdvertencia = 'Cargando ...';
  alertaInformacion = '';
  creandoCliente = {desplegado: true, nombre: '', apellido: ''};
  modificandoCliente = {desplegado: true, id: '', nombre: '', apellido: ''};
  modificandoCuenta = {desplegado: true, id: '', idCliente: '', intCuenta: null};

  constructor() { }

  ngOnInit() {

    this.ObtenerClientes();

  }

  async ObtenerClientes(){
    await fetch('http://localhost:8080/cliente',{
        method: 'GET',
        redirect: 'follow'
      })
      .then(response => response.json())
      .then((data) => {
        let clientesTemporal:any[] = [];
        data.map( (date) => {
          if(this.clientes.length !== date.id){
            for(let i = clientesTemporal.length; i < date.id ; i++){
              clientesTemporal.push('');
            }
          }
          date.desplegado = false;
          date.alerta = '';
          date.modificacion = false;
          clientesTemporal.push(date);
        });
        this.clientes = clientesTemporal;
        this.alertaAdvertencia = '';
      } )
      .catch(err => {
        this.alerta = err;
        this.alertaAdvertencia = '';
      });


    await new Promise((res, rej) => {
      if(this.clientes.length === 0 && this.alerta === ''){
        this.alerta = 'No hay clientes creados';
      }
      res();
    });
  }

  InformacionCuentas(cliente){
    this.alertaInformacion = '';
    if (this.clientes[cliente.id].desplegado){
      this.clientes[cliente.id].cuentas = [];
      this.clientes[cliente.id].desplegado = false;
      this.clientes[cliente.id].alerta = '';
    }else{
      fetch(`http://localhost:8080/cuenta/cliente/${cliente.id}`,{
        method: 'GET',
        redirect: 'follow'
      })
      .then(response => response.json())
      .then(data => {
        this.clientes[cliente.id].cuentas = data;
        this.alerta = '';
        this.alertaAdvertencia = '';
        this.clientes[cliente.id].desplegado = true;
        if(this.clientes[cliente.id].cuentas.length === 0){
          this.clientes[cliente.id].alerta = 'No tiene cuentas';
        }
      } )
      .catch(err => {
        this.alerta = err;
      });
    }
  }

  async crearCliente(evento){
    this.creandoCliente = {desplegado: true, nombre: '', apellido: ''};
    await fetch(`http://localhost:8080/cliente`,{
      method: 'POST',
      redirect: 'follow',
      body: JSON.stringify({
        intNombre: evento.target.nombre.value,
        intApellido: evento.target.apellido.value
      }),
      headers:{
        'Content-Type': 'application/json'
      }
    })
    .catch(err => this.alerta = err);
    await this.ObtenerClientes();
  }

  deplegarModificarCliente(cliente){
    if(this.modificandoCliente.id === cliente.id){
      this.modificandoCliente.id = '';
    }else{
      this.modificandoCliente = {desplegado: false, id: '', nombre: '', apellido: ''};
      this.modificandoCliente.id = cliente.id;
    }
  }

  async modificarCliente(evento, idCliente){
    await fetch(`http://localhost:8080/cliente`,{
      method: 'POST',
      redirect: 'follow',
      body: JSON.stringify({
        id: idCliente,
        intNombre: evento.target.nombre.value,
        intApellido: evento.target.apellido.value
      }),
      headers:{
        'Content-Type': 'application/json'
      }
    })
    .catch(err => this.alerta = err);
    await new Promise((res, rej) => {
      this.modificandoCliente = {desplegado: false, id: '', nombre: '', apellido: ''};
      res();
    });
    await this.ObtenerClientes();
  }

  async eliminarCliente(cliente){
    await fetch(`http://localhost:8080/cliente/${cliente.id}`,{
      method: 'DELETE',
      redirect: 'follow',
    })
    .catch(err => this.alerta = err);
    await this.ObtenerClientes();
  }

  deplegarCrearCuenta(cliente){
    if(this.modificandoCuenta.idCliente === cliente.id){
      this.modificandoCuenta.intCuenta = null;
      this.modificandoCuenta.idCliente = '';
    }else{
      this.modificandoCuenta = {desplegado: true, id: '', idCliente: '', intCuenta: null};
      this.modificandoCuenta.idCliente = cliente.id;
    }
  }

  async crearCuenta(evento, idCliente){
    await fetch(`http://localhost:8080/cuenta`,{
      method: 'POST',
      redirect: 'follow',
      body: JSON.stringify({
        idCliente: idCliente,
        intCuenta: evento.target.intCuenta.value
      }),
      headers:{
        'Content-Type': 'application/json'
      }
    })
    .catch(err => this.alerta = err);
    await new Promise((res, rej) => {
      this.modificandoCuenta.intCuenta = null;
      this.modificandoCuenta.idCliente = '';
      res();
    });
    await this.ObtenerClientes();
  }

  deplegarModificarCuenta(idCuenta){
    if(this.modificandoCuenta.id === idCuenta){
      this.modificandoCuenta = {desplegado: true, id: '', idCliente: '', intCuenta: null};
    }else{
      this.modificandoCuenta = {desplegado: true, id: '', idCliente: '', intCuenta: null};
      this.modificandoCuenta.id = idCuenta;
    }
  }

  async modificarCuenta(evento, idCliente, idCuenta){
    console.log(evento.target.intCuenta.value);
    console.log(idCuenta);
    await fetch(`http://localhost:8080/cuenta`,{
      method: 'POST',
      redirect: 'follow',
      body: JSON.stringify({
        id: idCuenta,
        idCliente: idCliente,
        intCuenta: evento.target.intCuenta.value
      }),
      headers:{
        'Content-Type': 'application/json'
      }
    })
    .catch(err => this.alerta = err);
    await new Promise((res, rej) => {
      this.modificandoCuenta.intCuenta = null;
      this.modificandoCuenta.idCliente = '';
      this.modificandoCuenta.id = '';
      res();
    });
    await this.ObtenerClientes();
  }

  async eliminarCuenta(idCliente, idCuenta){

    await fetch(`http://localhost:8080/cuenta/${idCuenta}`,{
      method: 'DELETE',
      redirect: 'follow'
    });

    await fetch(`http://localhost:8080/cuenta/cliente/${idCliente}`,{
      method: 'GET',
      redirect: 'follow'
    })
    .then(response => response.json())
    .then(data => {
      // Se aplican correcciones a [cliente.id - 1] debido a que con el Parche #1
      // las posiciones en el arreglo coincide con la id del cliente
      this.clientes[idCliente].cuentas = data;
    } );
  }

}
