package interinside.encargo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EncargoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EncargoApplication.class, args);
	}

}
