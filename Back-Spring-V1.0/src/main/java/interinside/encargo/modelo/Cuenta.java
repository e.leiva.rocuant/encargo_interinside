package interinside.encargo.modelo;

import java.io.Serializable;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name = "Cuenta")
public class Cuenta implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int Id;

  @Column(length = 200)
  private String idCliente;

  @Column(length = 200)
  private Integer intCuenta;

}