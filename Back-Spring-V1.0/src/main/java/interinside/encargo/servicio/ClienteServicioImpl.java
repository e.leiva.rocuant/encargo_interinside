package interinside.encargo.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import interinside.encargo.modelo.Cliente;
import interinside.encargo.modelo.Cuenta;

@Service
public class ClienteServicioImpl implements ClienteServicio {

  @Autowired
  public CuentaDAO cuentaDAO;

  @Autowired
  public ClienteDAO clienteDAO;

  @Override
  @Transactional(readOnly = true)
  public List<Cliente> listarClientes() {
    return (List<Cliente>) clienteDAO.findAll();
  }

  @Override
  @Transactional
  public void guardar(Cliente cliente) {
    clienteDAO.save(cliente);
  }

  @Override
  @Transactional
  public void eliminar(Cliente cliente) {
    clienteDAO.delete(cliente);
  }

  @Override
  @Transactional(readOnly = true)
  public Cliente encontrarCliente(Cliente cliente) {
    return clienteDAO.findById(cliente.getId()).orElse(null);
  }

  @Override
  @Transactional
  public Cliente modificarCliente(Cliente cliente) {
    return clienteDAO.save(cliente);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Cuenta> encontrarCuentas(String idClient) {
    return (List<Cuenta>) cuentaDAO.findByIdCliente(idClient);

  }

}