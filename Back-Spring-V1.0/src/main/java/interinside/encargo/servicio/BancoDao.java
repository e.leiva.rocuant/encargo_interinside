package interinside.encargo.servicio;

import org.springframework.data.repository.CrudRepository;

import interinside.encargo.modelo.Banco;

public interface BancoDao extends CrudRepository<Banco, Integer> {

}