package interinside.encargo.servicio;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import interinside.encargo.modelo.Cuenta;

public interface CuentaDAO extends CrudRepository<Cuenta, Integer> {
  public List<Cuenta> findByIdCliente(String idCliente);

  public List<Cuenta> deleteByIdCliente(String idCliente);
}