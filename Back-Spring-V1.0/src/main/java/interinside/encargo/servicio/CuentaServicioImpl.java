package interinside.encargo.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import interinside.encargo.modelo.Cuenta;

@Service
public class CuentaServicioImpl implements CuentaServicio {

  @Autowired
  private CuentaDAO cuentaDAO;

  @Override
  @Transactional
  public void guardar(Cuenta cuenta) {
    cuentaDAO.save(cuenta);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Cuenta> listarCuentas() {
    return (List<Cuenta>) cuentaDAO.findAll();
  }

  @Override
  public void eliminar(Cuenta cuenta) {
    cuentaDAO.delete(cuenta);
  }

  @Override
  @Transactional(readOnly = true)
  public Cuenta encontrarCuenta(Cuenta cuenta) {
    return cuentaDAO.findById(cuenta.getId()).orElse(null);
  }

  @Override
  @Transactional
  public Cuenta modificarCuenta(Cuenta cuenta) {
    return cuentaDAO.save(cuenta);
  }

  @Override
  @Transactional
  public void eliminarCuentas(String idCliente) {
    cuentaDAO.deleteByIdCliente(idCliente);
  }
}