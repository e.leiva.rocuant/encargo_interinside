package interinside.encargo.servicio;

import java.util.List;

import interinside.encargo.modelo.Cliente;
import interinside.encargo.modelo.Cuenta;

public interface ClienteServicio {

  public List<Cliente> listarClientes();

  public void guardar(Cliente cliente);

  public void eliminar(Cliente cliente);

  public Cliente encontrarCliente(Cliente cliente);

  public Cliente modificarCliente(Cliente cliente);

  public List<Cuenta> encontrarCuentas(String idCliente);
}