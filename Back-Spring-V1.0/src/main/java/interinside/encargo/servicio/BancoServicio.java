package interinside.encargo.servicio;

import java.util.List;

import interinside.encargo.modelo.Banco;

public interface BancoServicio {

  public List<Banco> motrarBancos();

  public String agregarBanco(Banco banco);
}