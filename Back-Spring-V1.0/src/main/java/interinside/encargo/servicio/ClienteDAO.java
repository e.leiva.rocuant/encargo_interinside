package interinside.encargo.servicio;

import org.springframework.data.repository.CrudRepository;

import interinside.encargo.modelo.Cliente;

public interface ClienteDAO extends CrudRepository<Cliente, Integer> {

}