package interinside.encargo.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import interinside.encargo.modelo.Banco;

@Service
public class BancoServicioImpl implements BancoServicio {

  @Autowired
  public BancoDao bancoDao;

  @Autowired
  public BancoServicio bancoServicio;

  @Override
  @Transactional
  public String agregarBanco(Banco banco) {
    bancoDao.save(banco);
    return "Agregado exitosamente";
  }

  @Override
  @Transactional(readOnly = true)
  public List<Banco> motrarBancos() {
    return (List<Banco>) bancoDao.findAll();
  };
}