package interinside.encargo.servicio;

import java.util.List;

import interinside.encargo.modelo.Cuenta;

public interface CuentaServicio {

  public List<Cuenta> listarCuentas();

  public void guardar(Cuenta cuenta);

  public void eliminar(Cuenta cuenta);

  public Cuenta encontrarCuenta(Cuenta cuenta);

  public Cuenta modificarCuenta(Cuenta cuenta);

  public void eliminarCuentas(String idCliente);
}