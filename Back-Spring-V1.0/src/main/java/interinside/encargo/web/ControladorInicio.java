package interinside.encargo.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import interinside.encargo.modelo.Banco;
import interinside.encargo.modelo.Cliente;
import interinside.encargo.modelo.Cuenta;
import interinside.encargo.servicio.BancoServicio;
import interinside.encargo.servicio.ClienteServicio;
import interinside.encargo.servicio.CuentaServicio;

@Controller
// Configura CORS para los endpoints de la clase
@CrossOrigin(origins = "*")
public class ControladorInicio {

  @Autowired
  private CuentaServicio cuentaServicio;

  @Autowired
  private ClienteServicio clienteServicio;

  @Autowired
  private BancoServicio bancoServicio;

  @GetMapping("/")
  @ResponseBody
  public String endpoints() {
    return "<h4>Endpoints disponibles:</h4><ul><li>/cuentas</li><li>/cuentas/:id</li><li>/cuenta/cliente/:id</li></ul>";
  }

  @GetMapping("/cuenta")
  @ResponseBody
  public ResponseEntity<List<Cuenta>> obtenerCuentas() {
    System.out.println(cuentaServicio.listarCuentas());
    return new ResponseEntity<List<Cuenta>>(cuentaServicio.listarCuentas(), HttpStatus.OK);
  }

  @GetMapping("/cuenta/{id}")
  @ResponseBody
  public ResponseEntity<Cuenta> obtenerCuenta(@PathVariable Integer id) {
    Cuenta idCuenta = new Cuenta();
    idCuenta.setId(id);
    if (cuentaServicio.encontrarCuenta(idCuenta) == null) {
      return new ResponseEntity<Cuenta>(HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<Cuenta>(cuentaServicio.encontrarCuenta(idCuenta), HttpStatus.OK);
  }

  @PostMapping("/cuenta")
  @ResponseBody
  public ResponseEntity<String> agregarCuenta(@RequestBody Cuenta cuenta) {
    Cliente idCliente = new Cliente();
    idCliente.setId(Integer.parseInt(cuenta.getIdCliente()));
    // Se verifica que los campos no esten vacios
    if (ControladorValidador.validCuentaJSON(cuenta)) {
      return new ResponseEntity<String>("No has ingresado los parametro correctos", HttpStatus.BAD_REQUEST);
      // Se verifica existencia del cliente para agregar cuenta
    } else if (clienteServicio.encontrarCliente(idCliente) == null) {
      return new ResponseEntity<String>("Necesitas indicar un id existente", HttpStatus.BAD_REQUEST);
    }
    cuentaServicio.guardar(cuenta);
    return new ResponseEntity<String>("Tu solicitud ha sido ingresada correctamente a la base de datos",
        HttpStatus.CREATED);
  }

  @DeleteMapping("/cuenta/{id}")
  @ResponseBody
  public ResponseEntity<String> eliminarCuenta(@PathVariable Integer id) {
    Cuenta idCuenta = new Cuenta();
    idCuenta.setId(id);
    if (cuentaServicio.encontrarCuenta(idCuenta) == null) {
      return new ResponseEntity<String>("La id que desea eliminar no existe", HttpStatus.BAD_REQUEST);
    }
    cuentaServicio.eliminar(idCuenta);
    return new ResponseEntity<String>("Elemento eliminado correctamente", HttpStatus.NO_CONTENT);
  }

  @PatchMapping("/cuenta/{id}")
  @ResponseBody
  public ResponseEntity<String> modificarCuenta(@PathVariable Integer id, @RequestBody Cuenta body) {
    Cuenta idCuenta = new Cuenta();
    idCuenta.setId(id);
    // para evitar modificaciones a objetos con id distinta
    body.setId(id);
    if (cuentaServicio.encontrarCuenta(idCuenta) == null) {
      return new ResponseEntity<String>("La id que desea modificar no existe", HttpStatus.BAD_REQUEST);
    } else if (ControladorValidador.validCuentaJSON(body)) {
      return new ResponseEntity<String>("No has ingresado los parametro correctos", HttpStatus.BAD_REQUEST);
    }
    cuentaServicio.modificarCuenta(body);
    return new ResponseEntity<String>("elemento Actualizado correctamente", HttpStatus.OK);
  }

  @GetMapping("/cuenta/cliente/{id}")
  @ResponseBody
  public ResponseEntity<List<Cuenta>> obtenerCuentas(@PathVariable String id) {
    return new ResponseEntity<List<Cuenta>>(clienteServicio.encontrarCuentas(id), HttpStatus.OK);
  }

  @GetMapping("/cliente")
  @ResponseBody
  public ResponseEntity<List<Cliente>> ObtenerClientes() {
    return new ResponseEntity<List<Cliente>>(clienteServicio.listarClientes(), HttpStatus.OK);
  }

  @GetMapping("/cliente/{id}")
  @ResponseBody
  public ResponseEntity<Cliente> ObtenerCliente(@PathVariable Integer id) {
    Cliente idCliente = new Cliente();
    idCliente.setId(id);
    if (clienteServicio.encontrarCliente(idCliente) == null) {
      return new ResponseEntity<Cliente>(HttpStatus.BAD_REQUEST);
    }
    return new ResponseEntity<Cliente>(clienteServicio.encontrarCliente(idCliente), HttpStatus.OK);
  }

  @PostMapping("/cliente")
  @ResponseBody
  public ResponseEntity<String> agregarCliente(@RequestBody Cliente cliente) {
    if (ControladorValidador.validClienteJSON(cliente)) {
      return new ResponseEntity<String>("No has ingresado los parametro correctos", HttpStatus.BAD_REQUEST);
    }
    clienteServicio.guardar(cliente);
    return new ResponseEntity<String>("Tu solicitud ha sido ingresada correctamente a la base de datos",
        HttpStatus.CREATED);
  }

  @DeleteMapping("/cliente/{id}")
  @ResponseBody
  public ResponseEntity<String> eliminarCliente(@PathVariable Integer id) {
    Cliente idCliente = new Cliente();
    idCliente.setId(id);
    if (clienteServicio.encontrarCliente(idCliente) == null) {
      return new ResponseEntity<String>("La id del cliente que desea eliminar no existe", HttpStatus.BAD_REQUEST);
    }
    // Elimina al cliente
    clienteServicio.eliminar(idCliente);
    // Elimina cuentas asociadas al cliente
    cuentaServicio.eliminarCuentas(String.valueOf(id));
    System.out.println("paso por aca");
    return new ResponseEntity<String>("Elemento eliminado correctamente", HttpStatus.NO_CONTENT);
  }

  @PatchMapping("/cliente/{id}")
  @ResponseBody
  public ResponseEntity<String> modificarCliente(@PathVariable Integer id, @RequestBody Cliente body) {
    Cliente idCliente = new Cliente();
    idCliente.setId(id);
    // para evitar modificaciones a objetos con id distinta
    body.setId(id);
    if (clienteServicio.encontrarCliente(idCliente) == null) {
      return new ResponseEntity<String>("La id que desea modificar no existe", HttpStatus.BAD_REQUEST);
    } else if (ControladorValidador.validClienteJSON(body)) {
      return new ResponseEntity<String>("No has ingresado los parametro correctos", HttpStatus.BAD_REQUEST);
    }
    clienteServicio.modificarCliente(body);
    return new ResponseEntity<String>("elemento Actualizado correctamente", HttpStatus.OK);
  }

  @GetMapping("/bancos")
  @ResponseBody
  public ResponseEntity<List<Banco>> listarBancos() {
    return new ResponseEntity<List<Banco>>(bancoServicio.motrarBancos(), HttpStatus.OK);
  }

  @PostMapping("/banco")
  @ResponseBody
  public ResponseEntity<String> agregarBanco(@RequestBody Banco banco) {
    bancoServicio.agregarBanco(banco);
    return new ResponseEntity<String>("Banco agregado exitosamente", HttpStatus.OK);
  }
}